FROM alpine

FROM alpine:latest

LABEL description A simple static HTTP server

RUN apk add --no-cache lighttpd \
   && rm -rf /var/www \
   && mkdir /var/www \
   && rm -rf /etc/lighttpd \
   && mkdir /etc/lighttpd

COPY lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY mime.conf /etc/lighttpd/mime.conf
COPY lighttpd-custom-conf.sh /usr/local/bin/lighttpd-custom-conf

RUN chmod +x /usr/local/bin/lighttpd-custom-conf

CMD [ "lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf" ]
