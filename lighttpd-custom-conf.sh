#! /usr/bin/env sh

main()
{
	if [ ! -d "/etc/lighttpd/custom.d" ]
	then
		return 0
	fi

	include_dir "/etc/lighttpd/custom.d"
}

include_dir()
{
	for entry in "$1"/*
	do
		if [ -d "$entry" ]
		then
			include_dir "$entry"
			continue
		fi

		cat -- "$entry"
	done
}

main "$@"
