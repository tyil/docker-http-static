# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [UNRELEASED]
### Added
- Additional logging is now logged to STDERR, so it can be viewed using
  Docker/Kubernetes logging functionalities.

## [0.1.0] - 2018-12-28
- Initial release
