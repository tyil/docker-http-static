# http-static

This is a small Docker container intended to host a small site. It hosts
whatever you put in `/var/www` as static content over port 80.

## License

All sources in this repository are made available under the terms of the GNU
Affero General Public License, version 3.0.
